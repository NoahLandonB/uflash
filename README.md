![UFlash USB Icon](Icon.png "UFlash USB Icon")

# UFlash
A USB flashing utility and ISO downloader. Easily download an ISO from the huge list of ISO files in the repository. All ISO's are valid and downloaded from the official ISO site.

# What Exactly Is UFlash?
UFlash is a handy tool made in C# that let's you easily flash a USB with an ISO file. For example, let's say I wanted to clean install my Windows 10 machine. I would download the Windows 10 ISO, open UFlash, configure my settings and select my drive, and just go.

UFlash supports Windows 10 and Windows 11. We do not guarentee support for Windows 7, 8, or 8.1. Windows XP and below are not supported and will fail to start.

# Is UFlash Safe?
Yes! UFlash is very safe and open-source to everyone. UFlash works in-hand with Windows and uses the proper methods that shouldn't flag any anti-virus. If an anti-virus flags UFlash, it is most likely a false positive. UFlash is not signed with a digital signature meaning it is not verified by a known publisher. This is due to the cost of a digital signature.

# Is UFlash Free?
Yes! If you paid for UFlash then you have been scammed and should refund your purchase. UFlash is free software licensed under the GPL-3.0 license. UFlash can be used commercially, personally, and privately. UFlash and the developer(s) behind the project will not limit your usage or make you purchase a license.
